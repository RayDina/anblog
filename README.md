# Anblog

## 简介

本博客系统由php+mysql开发，使用amazeui的web前端框架，由于初次开始写web程序，错误之处在所难免，敬请见谅。

## 关于我

* 新浪微博: [@安生亦梦](http://weibo.com/iDina)
* BLOG： [@安生亦梦](http://www.idina.cn/)

## 版权协议


Anblog系统由安生亦梦创作，采用 [Mozilla Public License, version 2.0](https://www.mozilla.org/MPL/2.0/) 进行许可。

