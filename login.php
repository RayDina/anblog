<?php 
	//用户登陆
	session_start();
	require_once "./config/config.php";
	require_once "./include/db.class.php";

	$error_msg = "";
	//如果用户未登录，即未设置$_SESSION['user_id']时，执行以下代码
	if(!isset($_SESSION['uid'])){
    	if(isset($_POST['submit'])){
    		//用户提交登录表单时执行如下代码
			$mail = $_POST['mail'];
			$passwd = $_POST['passwd'];
			$db = new an_db();
			$mail = $db->escape_string($_POST['mail']);
			$passwd = $db->escape_string($_POST['passwd']);
			//判空
			if(!empty($mail)&&!empty($passwd)){
				//非空判断正确否
				$sql = "SELECT uid,name,isadmin FROM an_user WHERE mail = '{$mail}' AND password = '".md5($passwd)."'";
				$sql_value= $db->query($sql);
				//用用户名和密码进行查询，若查到的记录正好为一条，则设置session和cookie，同时进行页面重定向
				if($sql_value && $sql_value->num_rows==1){
					$rows=$sql_value->fetch_assoc();
					//设置session
					$_SESSION['uid']=$rows['uid'];
					$_SESSION['name']=$rows['name'];
					$_SESSION['isadmin']=$rows['isadmin'];
					//设置cookie
					setcookie('uid',$rows['uid'],time()+(60*60));
					setcookie('name',$rows['name'],time()+(60*60));
					//页面重定向
					if ($rows['isadmin'] == 1){
						//管理员
						$home_url = './admin/admin_index.php';
                		header('Location: '.$home_url);
					}else{
						//普通用户
						$home_url = './admin/admin_index.php';
                		header('Location: '.$home_url);
					}
				}else{
					$error_msg = '登陆失败，请检查账户密码是否正确。';
				}

			}else{
				$error_msg = '登陆失败，请检查账户密码是否正确。';
			}
		}
	}else{
		//如果用户已经登陆，直接跳转
		if (isset($_SESSION['isadmin'])){
			if($_SESSION['isadmin']){
				//管理员
				$home_url = './admin/admin_index.php';
			}else{
				//普通用户
				$home_url = './admin/admin_index.php';
			}
			header('Location: '.$home_url);
		}else{
			//session信息不完整，让用户登陆
			$home_url = './admin/loginout.php';
			header('Location: '.$home_url);
		}
	}
?>
<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>Anblog Sign In</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="alternate icon" type="image/png" href="assets/i/favicon.png">
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/app.css"/>
  <script type="text/javascript" src="assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/amazeui.min.js"></script>
  <script>
    $(function(){
      var progress = $.AMUI.progress;

      $('#np-s').on('click', function() {
          progress.start();
      });

      $('#np-d').on('click', function() {
        progress.done();
      });
    });
  </script>
</head>
<body>
<div class="header">
  <div class="am-g">
    <h1>Anblog Sign In</h1>
    <p>安生、亦梦，开源博客系统 v0.2</p>
  </div>
  <hr />
</div>
<div class="am-g">
  <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
    <br>
    <?php
    if(!isset($_SESSION['uid'])){
        echo '<p>'.$error_msg.'</p>';
    ?>
    <form method="post" class="am-form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
      <label for="email">Email:</label>
      <input type="email" name="mail" id="email" value="<?php if(!empty($mail)) echo $mail; ?>">
      <br>
      <label for="password">Password:</label>
      <input type="password" name="passwd" id="password" value="">
      <br>
      <label for="remember-me">
        <input id="remember-me" type="checkbox">
        记住密码
      </label>
      <br />
      <div class="am-cf">
        <input type="submit" name="submit" value="登 录" class="am-btn am-btn-primary am-btn-sm am-fl" id="login_submit">
        <input type="button" name="forgave" value="忘记密码 ^_^? " class="am-btn am-btn-default am-btn-sm am-fr" onclick="window.location='./forgetpwd.html'">
      </div>
    </form>
    <?php
    }
    ?>
    <hr>
    <p>Copyright © 2015 WangBo <a href="http://www.idina.cn" target="_blank"> iDina.cn </a> All rights reserved. </p>
  </div>
</div>
</body>
</html>
