<?php

/*
 * Anblog v0.1
 * config.php
 * */

/*
 *  博客基础信息
 * */

//博客名称
$SiteName="安生亦梦";
//网站副标题
$SiteAbout="安生亦梦，技术分享站点";
//关键字
$SiteKeywords=null;

/*
 * MySQL数据库设置
 * */
// MySQL 主机
define('AN_DB_HOST', 'localhost');
// MySQL 端口
define('AN_DB_PORT', 3306);
// MySQL 数据库名
define('AN_DB_NAME', 'anblog');
// MySQL 数据库用户名
define('AN_DB_USER', 'anblog');
// MySQL 数据库密码
define('AN_DB_PASSWORD', 'anblog');


?>
