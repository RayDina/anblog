<?php 

//展示页面
//展示博文信息
if (!isset($_GET['pid'])){
	header("Location:index.php");
}
$pid = $_GET['pid'];
//展示文章详情
require_once './config/config.php';
require_once './include/db.class.php';
$db = new an_db();
$sql = "select an_posts.pid,an_posts.title,an_user.name,an_posts.content,an_posts.date,an_posts.tag from an_user inner join an_posts on an_posts.uid = an_user.uid and an_posts.isdraft=0 and an_posts.pid=".$pid;
$result = $db->query($sql);
//判断结果集中数目是不是大于0
if ($result && $result->num_rows > 0){
  //fetch_assoc
  $rows = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>AnBlog</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp"/>
  <link rel="alternate icon" type="image/png" href="assets/i/favicon.png">
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <style>
    @media only screen and (min-width: 1200px) {
      .blog-g-fixed {
        max-width: 1200px;
      }
    }

    @media only screen and (min-width: 641px) {
      .blog-sidebar {
        font-size: 1.4rem;
      }
    }

    .blog-main {
      padding: 20px 0;
    }

    .blog-title {
      margin: 40px 0 20px 0;
      text-align: center;
    }

    .blog-meta {
      font-size: 14px;
      margin: 10px 0 20px 0;
      text-align: center;
    }

    .blog-meta a {
      color: #27ae60;
    }

    .blog-pagination a {
      font-size: 1.4rem;
    }

    .blog-team li {
      padding: 4px;
    }

    .blog-team img {
      margin-bottom: 0;
    }

    .blog-content img,
    .blog-team img {
      max-width: 100%;
      height: auto;
    }

    .blog-footer {
      padding: 10px 0;
      text-align: center;
    }
  </style>
</head>
<body>
<?php
//导航栏
require_once './include/header.php';
?>
<div class="am-g am-g-fixed blog-g-fixed">
<div class="am-u-md-8">
<article class="am-article">
    <div class="am-article-hd">
        <h1 class="am-article-title blog-title"><?php echo $rows['title'] ?></h1>
        <p class="am-article-meta blog-meta"><?php echo $rows['name'] ?></p>
    </div>
<hr class="am-article-divider"/>
    <div class="am-g">
      <div class="am-u-sm-11 am-u-sm-centered">
        <div class="am-cf am-article">
          <div>
          	<?php echo $rows['content']; } else { echo "<script>location.href='./index.php'</script>"; } ?>
          </div>
        <hr/>
        <h3>评论</h3>
        <ul class="am-comments-list">
        <?php 
        	//连接数据库查询评论
        	$sql = "select * from an_comment where pid=".$pid;
        	$result = $db->query($sql);
        	if ($result && $result->num_rows > 0){
        		while($rows = $result->fetch_assoc()){ 
        			$usergravatar = 'http://gravatar.duoshuo.com/avatar/'.md5($rows['mail']).'?s=48';
        	?>
					<li class="am-comment">
					<a href="#">
					  <img src="<?php echo $usergravatar; ?>" class="am-comment-avatar" width="48" height="48">
					</a>
					<div class="am-comment-main">
					  <header class="am-comment-hd">
					    <div class="am-comment-meta">
					      <a href="#link-to-user" class="am-comment-author"><?php echo $rows['name']; ?></a> 评论于 <?php echo $rows['time']; ?>
					    </div>
					  </header>
					  <div class="am-comment-bd">
					    <p><?php echo $rows['text']; ?></p>
					  </div>
					</div>
					</li>
		<?php
        		}
        	}else{
        		echo "暂时还有没有评论哦~~~";
        	}
        ?>
        </ul>
        <hr />
<div class="doc-example">
  <form action="./admin/comment_doaction.php?act=addcom&pid=<?php echo $pid; ?>" class="am-form" method="post">
    <fieldset>
      <legend>发表评论</legend>
      <div class="am-form-group">
        <label for="doc-vld-name-1">姓名：</label>
        <input type="text" id="doc-vld-name-1" maxlength="20" placeholder="请输入姓名" class="am-form-field" required="" name="name"></div>
        <div class="am-form-group">
          <label for="doc-vld-email-1">邮箱：</label>
          <input type="email" id="doc-vld-email-1" placeholder="请输入邮箱" required="" name="mail"></div>
          <div class="am-form-group">
            <div class="am-form-group">
              <label for="doc-vld-ta-1">评论：</label>
              <textarea id="doc-vld-ta-1" minlength="10" maxlength="200" name="text" placeholder="您的评论哦~~"></textarea>
            </div>
            <button class="am-btn am-btn-secondary" type="submit">提交</button>
          </fieldset>
        </form>
      </div>









  </div>
</div>
</div>
</div>


<?php
//右边栏
require_once './include/sidebar.php';
//底边栏
require_once './include/footer.php';

?>

