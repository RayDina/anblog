<?php
	require_once 'session.inc.php';
	//用户操作执行界面
	header('content-type:text/html;charset=utf-8');
	//连接数据库操作
	require_once "../config/config.php";
	require_once "../include/db.class.php";

	$an_db = new an_db();

	//POST接受的数据
	if (isset($_POST['title'])){
		//title
		$title=$_POST['title'];
		$title=$an_db->escape_string($title);
		//url
		$url=$_POST['url'];
		//$url=$an_db->escape_string($url);
		//other
		$other=$_POST['other'];
		$other=$an_db->escape_string($other);
	}
	if (isset($_GET['lid'])){
		$lid=$_GET['lid'];
	}
	//获取行为
	$act=$_GET['act'];

	//根据行为，完成操作

	switch($act){
		case 'addlink':
			//添加
			$sql="INSERT INTO `an_links` (`lid`, `title`, `url`, `other`)values(NULL,'{$title}','{$url}','{$other}')";
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//添加成功
				echo "<script type='text/javascript'>
						alert('添加成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('添加失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
		case 'updatelink':
			//修改
			$sql = "UPDATE an_links SET `title` = '{$title}',`url` = '{$url}',`other` = '{$other}' WHERE lid=".$lid;
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//修改成功
				echo "<script type='text/javascript'>
						alert('修改成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('修改失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
		case 'dellink':
			//删除
			$sql="delete from an_links where an_links.lid=".$lid;
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//删除成功
				echo "<script type='text/javascript'>
						alert('删除成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('删除失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
	}	

?>