<div class="am-cf admin-main">
<!-- sidebar start -->
  <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
    <div class="am-offcanvas-bar admin-offcanvas-bar">
      <ul class="am-list admin-sidebar-list">
        <li><a href="admin_index.php"><span class="am-icon-home"></span> 首页</a></li>
        
        <li class="admin-parent">
          <a class="am-cf am-collapsed" data-am-collapse="{target: '#collapse-nav-manager'}">
            <span class="am-icon-pencil-square-o"></span> 文章管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class="am-list admin-sidebar-sub am-collapse" id="collapse-nav-manager">
            <li><a href="admin_blog_add.php"><span class="am-icon-file-text"></span> 添加博文 </a></li>
            <li><a href="admin_blog_manager.php"><span class="am-icon-table"></span> 查看博文 </a></li>
            <li><a href="admin_comment_manager.php"><span class="am-icon-th"></span> 查看评论 </a></li>
          </ul>
        </li>
        <?php
        if(isset($_SESSION['isadmin']) && ($_SESSION['isadmin'] == 1)){
        
        ?>
        <!--li><a href="#"><span class="am-icon-cogs"></span> 网站设置</a></li-->

        <li class="admin-parent">
          <a class="am-cf am-collapsed" data-am-collapse="{target: '#collapse-nav-setup'}">
            <span class="am-icon-group"></span> 用户管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class="am-list admin-sidebar-sub am-collapse" id="collapse-nav-setup">
            <li><a href="admin_user_manager.php" class="am-cf"><span class="am-icon-user"></span> 用户列表</a></li>
            <li><a href="admin_user_add.php"><span class="am-icon-user-plus"></span> 添加用户 </a></li>
          </ul>
        </li>

        <li><a href="admin-log.html"><span class="am-icon-calendar"></span> 系统日志</a></li>
        
        <li class="admin-parent">
          <a class="am-cf am-collapsed" data-am-collapse="{target: '#collapse-nav-link'}">
            <span class="am-icon-list-alt"></span> 友链管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class="am-list admin-sidebar-sub am-collapse" id="collapse-nav-link">
            <li><a href="admin_link_manager.php" class="am-cf"><span class="am-icon-link"></span> 查看友链 </a></li>
            <li><a href="admin_link_add.php"><span class="am-icon-th"></span> 添加友链 </a></li>
          </ul>
        </li>
        <?php } ?>
        <li><a href="admin_help.php" target="admin_content"><span class="am-icon-puzzle-piece"></span> 帮助页</a></li>

        <li><a href="#"><span class="am-icon-sign-out"></span> 注销</a></li>

      </ul>

      <div class="am-panel am-panel-default admin-sidebar-panel">
        <div class="am-panel-bd">
          <p><span class="am-icon-bookmark"></span> 公告</p>
          <p>时光静好，细水流年。—— AnBlog</p>
        </div>
      </div>
    </div>
  </div>
  <!-- sidebar end -->