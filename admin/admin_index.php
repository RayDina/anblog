<?php
/**
 * Created by PhpStorm.
 * User: wangbo
 * Date: 2015/6/9
 * Time: 9:51
 */

require_once './admin_frame.php';

    //单独连接其他数据库，所以不使用类an_db
    $db = new mysqli(AN_DB_HOST, AN_DB_USER, AN_DB_PASSWORD, "information_schema", AN_DB_PORT);
    if (mysqli_connect_errno())
    {
        echo "数据库连接失败".mysqli_connect_errno().mysqli_connect_error();
        exit();
    }
    $db->set_charset('utf8');

    $sql="select table_name,table_rows from tables where TABLE_SCHEMA = 'anblog' order by table_rows desc";
    $sql_value=$db->query($sql);
    if(!$sql_value){
        //异常，数据库没有初始化。
    }
?>
<div class="admin-content">
  <div class="am-cf am-padding">
    <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">首页</strong> / <small>信息统计</small></div>
  </div>
<?php
    if($sql_value && $sql_value->num_rows>0){
      echo '<ul class="am-avg-sm-1 am-avg-md-4 am-margin am-padding am-text-center admin-content-list ">';
      while($rows=$sql_value->fetch_assoc()){
        switch ($rows['table_name']){
          case 'an_user':
          if ($_SESSION['isadmin'] == 1)
            echo '<li><a href="admin_user_manager.php" class="am-text-secondary"><span class="am-icon-btn am-icon-user-md"></span><br/>用户统计<br/>'.$rows['table_rows'].'</a></li>';
            break;
          case 'an_posts':
            echo '<li><a href="admin_blog_manager.php" class="am-text-success"><span class="am-icon-btn am-icon-file-text"></span><br/>文章统计<br/>'.$rows['table_rows'].'</a></li>';
            break;
          case 'an_links':
            if ($_SESSION['isadmin'] == 1)
            echo '<li><a href="admin_link_manager.php" class="am-text-warning"><span class="am-icon-btn am-icon-briefcase"></span><br/>友链统计<br/>'.$rows['table_rows'].'</a></li>';
            break;
          case 'an_comment':
            echo '<li><a href="admin_comment_manager.php" class="am-text-danger"><span class="am-icon-btn am-icon-recycle"></span><br/>评论统计<br/>'.$rows['table_rows'].'</a></li>';
            break;
        }
      }
      echo '</ul>';
    }
  ?>
    

    <div class="am-g">
      <div class="am-u-md-6">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-2'}">博客系统信息<span class="am-icon-chevron-down am-fr" ></span></div>
          <div id="collapse-panel-2" class="am-in">
            <table class="am-table am-table-bd am-table-bdrs am-table-striped am-table-hover">
              <tbody>
              <tr>
                <td>博客版本</td>
                <td>Anblog version 0.5.3</td>
              </tr>
              <tr>
                <td>开发者</td>
                <td>王博</td>
              </tr>
              <tr>
                <td>日期</td>
                <td>2015/06/17</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="am-u-md-6">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-4'}">服务器信息<span class="am-icon-chevron-down am-fr" ></span></div>
          <div id="collapse-panel-4" class="am-panel-bd am-collapse am-in">
            <ul class="am-list admin-content-task">
              <li>
                <div class="admin-task-meta"> Web服务器信息 </div>
                <div class="admin-task-bd">
                  <?php echo $_SERVER['SERVER_SOFTWARE']; ?>
                </div>
              </li>
              <li>
                <div class="admin-task-meta"> 主机信息 </div>
                <div class="admin-task-bd">
                  <?php echo $_SERVER['SERVER_NAME'];?>(<?php if('/'==DIRECTORY_SEPARATOR){echo $_SERVER['SERVER_ADDR'];}else{echo @gethostbyname($_SERVER['SERVER_NAME']);} ?>)
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
<?php
    //加载底部
    require_once './admin_frame_end.php';
?>
