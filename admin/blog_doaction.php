<?php
	require_once 'session.inc.php';
	//用户操作执行界面
	header('content-type:text/html;charset=utf-8');
	//连接数据库操作
	require_once "../config/config.php";
	require_once "../include/db.class.php";

	$an_db = new an_db();

	//POST接受的数据
	if (isset($_POST['title'])){
		//title
		$title=$_POST['title'];
		$title=$an_db->escape_string($title);
		//uid
		$uid=$_POST['uid'];
		//tag
		$tag=$_POST['tag'];
		$tag=$an_db->escape_string($tag);
		//about
		$about=$_POST['about'];
		$about=$an_db->escape_string($about);
		//草稿
		$isdraft=$_POST['isdraft'];
		$content = $_POST['content'];
		$content=$an_db->escape_string($content);
	}
	if (isset($_GET['pid'])){
		$uid=$_GET['pid'];
	}

	//获取行为
	$act = $_GET['act'];

	//根据行为，完成操作

	switch($act){
		case 'addblog':
			//添加文章
			$sql="INSERT INTO `an_posts` (`pid`,`uid`, `title`, `about`, `content`, `isdraft`, `tag`, `date`)values(NULL,\"{$uid}\",\"{$title}\",\"{$about}\",\"{$content}\",\"{$isdraft}\",\"{$tag}\",CURDATE())";
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//添加成功
				echo "<script type='text/javascript'>
						alert('添加成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('添加失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
		case 'updateblog':
			//修改文章
			$sql = "UPDATE `an_posts` SET `title` = '{$title}',`about` = '{$about}',`content` = '{$content}',`isdraft` = '{$isdraft}',`tag` = '{$tag}', `date` = CURDATE() WHERE `an_posts`.`pid` = ".$pid;
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//修改成功
				echo "<script type='text/javascript'>
						alert('修改成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('修改失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
		case 'delblog':
			//删除文章
			$sql="delete from an_posts where pid=".$pid;
			$sql_value=$an_db->query($sql);
			if($sql_value){
				//删除成功
				echo "<script type='text/javascript'>
						alert('删除成功');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}else{
					echo "<script type='text/javascript'>
						alert('删除失败');
						location.href='".$_SERVER['HTTP_REFERER']."';
					</script>";
			}
			break;
	}	

?>