<?php
/**
 * Created by PhpStorm.
 * User: wangbo
 * Date: 2015/6/15
 * Time: 15:51
 */

require_once './admin_frame.php';
?>
  <!-- content start -->
  <div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">帮助页面</strong> / <small>Help Page</small></div>
    </div>

    <hr/>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-sm-centered">
        <h2>Anblog 开源博客系统</h2>
        <p>Anblog的诞生，依托于 GitHub 及其他技术社区上一些优秀的资源；Web前端使用了Amaze UI框架，特别感谢。</p>
        <p>感谢开源！感谢有你！</p>
        <hr/>
      </div>

      <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4">
        <h3>Bug 反馈</h3>
        <p>感谢您对Anblog 开源博客系统的关注和支持，如遇到 Bug 或者使用问题，可以通过以下途径反馈给我们：</p>
        <ol>
          <li>通过Coding上的项目 <a href="https://coding.net/u/idina/p/Anblog">https://coding.net/u/idina/p/Anblog</a>。</li>
          <li>通过我的博客提交 <a href="http://www.iDina.cn">iDina.cn</a>。</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- content end -->

<?php
    //加载底部
    require_once './admin_frame_end.php';
?>
