<?php
//评论管理
require_once './admin_frame.php';

$db = new an_db();

$sql ="select an_comment.*,an_posts.title from an_comment inner join an_posts on an_posts.pid = an_comment.pid";

$result = $db->query($sql);
?>
<div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">评论管理</strong> / <small>Comment Manager</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <!--buttom class="am-btn am-btn-default" href="admin_link_add.php"><span class="am-icon-plus"></span> 新增</buttom-->
          </div>
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-input-group am-input-group-sm">
          <input type="text" class="am-form-field">
          <span class="am-input-group-btn">
            <button class="am-btn am-btn-default" type="button">搜索</button>
          </span>
        </div>
      </div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-check"><input type="checkbox"></th><th class="table-id">ID</th><th>文章名称</th><th>姓名</th><th>邮箱</th><th>时间</th><th class="table-set">操作</th>
              </tr>
            </thead>
          <tbody>
          <?php
            if($result && $result->num_rows>0) {
              while ($rows=$result->fetch_assoc()) {
          ?>
          			<tr>
                  <td><input type="checkbox"></td>
                  <td><?php echo $rows['cid']; ?></td>
                  <td><?php echo $rows['title']; ?></td>
                  <td><?php echo $rows['name']; ?></td>
                  <td><?php echo $rows['mail']; ?></td>
                  <td class="am-hide-sm-only"><?php echo $rows['time']; ?></td>
                  <td>
                    <div class="am-btn-toolbar">
                        <a class="am-btn am-btn-default am-btn-xs" href="comment_doaction.php?act=delcomm&cid=<?php echo $rows['cid']; ?>"><span class="am-icon-trash-o"></span> 删除 </a>
                    </div>
                  </td>
                </tr>
			   <?php }	?>
          </tbody>
        </table>
          <div class="am-cf">
  共 <?php echo $result->num_rows; ?> 条记录
<?php
  if($result->num_rows > 20) { ?>
    <div class="am-fr">
      <ul class="am-pagination am-pagination-right">
        <li><a href="">&laquo; 上一页</a></li>
        <li><a href="">下一页 &raquo;</a></li>
      </ul>
    </div>
<?php 
  }
} else {
  echo "<script type='text/javascript'>alert(\"数据库暂时没有友情链接信息。\");</script>";
} ?>
</div>
          <hr>
        </form>
      </div>

    </div>
  </div>
  <?php require_once './admin_frame_end.php';?>