<?php
//添加用户
require_once './admin_frame.php';

?>

  <!-- content start -->
  <div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">添加用户</strong> / <small>Add User</small></div>
    </div>

    <hr/>

    <div class="am-g">

      <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4" style="position: initial;">
        <form class="am-form am-form-horizontal" action="user_doaction.php?act=addUser" method="post">
          <div class="am-form-group">
            <label for="user-name" class="am-u-sm-3 am-form-label">姓名 / Name</label>
            <div class="am-u-sm-9">
              <input type="text" id="user-name" name="name" placeholder="姓名 / Name">
              <small>输入你的名字，让我们记住你。</small>
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-email" class="am-u-sm-3 am-form-label">电子邮件 / Email</label>
            <div class="am-u-sm-9">
              <input type="email" id="user-email" name="mail" placeholder="输入你的电子邮件 / Email">
              <small>邮箱你懂得...</small>
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-passwd" class="am-u-sm-3 am-form-label">密码 / Password</label>
            <div class="am-u-sm-9">
              <input type="password" id="user-passwd" name="password" placeholder="输入你的密码 / Password">
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-sex" class="am-u-sm-3 am-form-label">性别 / Sex</label>
            <div class="am-u-sm-9">
              <div class="am-radio-inline">
                <label>
                  <input type="radio" name="sex" value="1" checked>
                  帅哥
                </label>
              </div>

              <div class="am-radio-inline">
                <label>
                  <input type="radio" name="sex" value="0" >
                  美女
                </label>
              </div>
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-weibo" class="am-u-sm-3 am-form-label">用户组 / Group</label>
            <div class="am-u-sm-9">
              <div class="am-radio-inline">
                <label>
                  <input type="radio" name="isadmin" value="1">
                  管理员
                </label>
              </div>

              <div class="am-radio-inline">
                <label>
                  <input type="radio" name="isadmin" value="0" checked>
                  普通用户
                </label>
              </div>
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-intro" class="am-u-sm-3 am-form-label">简介 / Intro</label>
            <div class="am-u-sm-9">
              <textarea name="about" class="" rows="5" id="user-intro" placeholder="输入个人简介"></textarea>
              <small>200字以内写出你的一生...</small>
            </div>
          </div>

          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary">提交</button>
              <button type="reset" class="am-btn am-btn-primary">重置</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- content end -->
<?php require_once './admin_frame_end.php';?>