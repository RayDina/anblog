<?php
//使用会话内存储的变量值之前必须先开启会话
session_start();
//如果会话没有被设置，查看是否设置了cookie
if(!isset($_SESSION['uid'])){
    if(isset($_COOKIE['uid'])&&isset($_COOKIE['name'])){
        //用cookie给session赋值
        $_SESSION['uid']=$_COOKIE['uid'];
        $_SESSION['name']=$_COOKIE['name'];
        //尝试获取权限

    }else{
    	//没有权限你来干嘛，去登陆吧~~~
    	$home_url = './loginout.php';
		header('Location: '.$home_url);
    }
}
