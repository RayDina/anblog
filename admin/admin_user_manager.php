<?php 
	//用户管理
	require_once './admin_frame.php';
	$an_db = new an_db();
	$sql_value= $an_db->query("SELECT * from an_user;");
?>

<div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">用户管理</strong> / <small>user manager</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <a class="am-btn am-btn-default" href="admin_user_add.php"><span class="am-icon-plus"></span> 新增</a>
          </div>
        </div>
      </div>
      <!--div class="am-u-sm-12 am-u-md-3">
        <div class="am-form-group">
          <select data-am-selected="{btnSize: 'sm'}" style="display: none;">
            <option value="option1">所有类别</option>
            <option value="option2">IT业界</option>
            <option value="option3">Linux</option>
          </select><div class="am-selected am-dropdown " id="am-selected-8fuuo" data-am-dropdown="">  <button type="button" class="am-selected-btn am-btn am-dropdown-toggle am-btn-sm am-btn-default">    <span class="am-selected-status am-fl">所有类别</span>    <i class="am-selected-icon am-icon-caret-down"></i>  </button>  <div class="am-selected-content am-dropdown-content">    <h2 class="am-selected-header"><span class="am-icon-chevron-left">返回</span></h2>       <ul class="am-selected-list">                     <li class="am-checked" data-index="0" data-group="0" data-value="option1">         <span class="am-selected-text">所有类别</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="1" data-group="0" data-value="option2">         <span class="am-selected-text">IT业界</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="2" data-group="0" data-value="option3">         <span class="am-selected-text">数码产品</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="3" data-group="0" data-value="option3">         <span class="am-selected-text">笔记本电脑</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="4" data-group="0" data-value="option3">         <span class="am-selected-text">平板电脑</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="5" data-group="0" data-value="option3">         <span class="am-selected-text">只能手机</span>         <i class="am-icon-check"></i></li>                                 <li class="" data-index="6" data-group="0" data-value="option3">         <span class="am-selected-text">超极本</span>         <i class="am-icon-check"></i></li>            </ul>    <div class="am-selected-hint"></div>  </div></div>
        </div>
      </div-->
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-input-group am-input-group-sm">
          <input type="text" class="am-form-field">
          <span class="am-input-group-btn">
            <button class="am-btn am-btn-default" type="button">搜索</button>
          </span>
        </div>
      </div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-check"><input type="checkbox"></th><th class="table-id">ID</th><th class="table-title">姓名</th><th class="table-type">用户组</th><th class="table-author am-hide-sm-only">邮箱</th><th class="table-date am-hide-sm-only">密码</th><th class="table-set">操作</th>
              </tr>
          </thead>
          <tbody>
			<?php 
			if($sql_value && $sql_value->num_rows>0){
				while($rows=$sql_value->fetch_assoc()){
					//print_r($rows);
			?>
			<tr>
        <td><input type="checkbox"></td>
        <td><?php echo $rows['uid']; ?></td>
        <td><?php echo $rows['name']; ?></td>
        <td><?php if($rows['isadmin']) echo "管理员"; else echo "普通用户"; ?></td>
        <td class="am-hide-sm-only"><?php echo $rows['mail']; ?></td>
        <td class="am-hide-sm-only"><?php echo $rows['password']; ?></td>
        <td>
          <div class="am-btn-toolbar">
            <div class="am-btn-group am-btn-group-xs">
              <a class="am-btn am-btn-default am-btn-xs am-text-secondary" href="admin_user_edit.php?id=<?php echo $rows['uid'] ?>"><span class="am-icon-pencil-square-o"></span> 编辑</a>
              <a class="am-btn am-btn-default am-btn-xs" href="user_doaction.php?act=delUser&uid=<?php echo $rows['uid']; ?>"><span class="am-icon-trash-o"></span> 删除 </a>
            </div>
          </div>
        </td>
      </tr>
			<?php }	?>
          </tbody>
        </table>
          <div class="am-cf">
  共 <?php echo $sql_value->num_rows; ?> 条记录
<?php
  if($sql_value->num_rows > 20) { ?>
  <div class="am-fr">
    <ul class="am-pagination am-pagination-right">
      <li><a href="">&laquo; 上一页</a></li>
      <li><a href="">下一页 &raquo;</a></li>
    </ul>
  </div>
  <?php } ?>
  <?php }else{echo "数据库查询失败。";}?>
</div>
          <hr>
          <p>注：密码为一次MD5加密后密码。</p>
        </form>
      </div>

    </div>
  </div>
  <?php require_once './admin_frame_end.php';?>