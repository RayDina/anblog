<?php
//添加用户
require_once './admin_frame.php';

?>

  <!-- content start -->
  <div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">添加友链</strong> / <small>Add link</small></div>
    </div>

    <hr/>

    <div class="am-g">

      <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4" style="position: initial;">
        <form class="am-form am-form-horizontal" action="link_doaction.php?act=addlink" method="post">
          <div class="am-form-group">
            <label for="user-name" class="am-u-sm-3 am-form-label">名称 / title</label>
            <div class="am-u-sm-9">
              <input type="text" id="user-name" name="title" placeholder="名称 / title">
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-passwd" class="am-u-sm-3 am-form-label">网址 / URL</label>
            <div class="am-u-sm-9">
              <input type="text" name="url" placeholder="网址 / URL">
            </div>
          </div>

          <div class="am-form-group">
            <label for="user-intro" class="am-u-sm-3 am-form-label">备注 / other</label>
            <div class="am-u-sm-9">
              <input type="text" name="other" class="" rows="5" id="user-intro" placeholder="备注 / other">
            </div>
          </div>

          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary">提交</button>
              <button type="reset" class="am-btn am-btn-primary">重置</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- content end -->
<?php require_once './admin_frame_end.php';?>