<?php
/**
 * Created by PhpStorm.
 * User: wangbo
 * Date: 2015/6/9
 * Time: 9:51
 */
//权限判断
require_once 'session.inc.php';
require_once '../config/config.php';
require_once '../include/db.class.php';

if(isset($_GET['pid'])){
  $pid = $_GET['pid'];
}else{
  echo "<script type='text/javascript'>alert(\"参数不正确。\");</script>";
  exit;
}

$db = new an_db();

$result = $db->query("SELECT * from an_posts where pid=".$pid);

if($result && $result->num_rows>0){
  $rows=$result->fetch_assoc();
}else{
  echo "<script type='text/javascript'>alert(\"数据异常，请检查。\");</script>";
  exit();
}


 ?>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AnBlog Manager System</title>
  <meta name="description" content="AnBlog Manager System">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="../assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="../assets/css/amazeui.min.css"/>
  <link href="http://cdn.staticfile.org/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets/css/jquery.qeditor.css" type="text/css">
  <link rel="stylesheet" href="../assets/css/admin.css">
  <link rel="stylesheet" href="../assets/css/app.css">
  <style type="text/css" media="screen">
  .textarea {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -webkit-transition: border linear .2s, box-shadow linear .2s;
    -moz-transition: border linear .2s, box-shadow linear .2s;
    -o-transition: border linear .2s, box-shadow linear .2s;
    transition: border linear .2s, box-shadow linear .2s;
    padding: 4px 6px;
    font-size: 14px;
    line-height: 20px;
    color: #555555;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    vertical-align: middle;
    outline: none;
    height: 400px;
  }
  </style>
  <script src="http://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
  <script src="../assets/js/jquery.qeditor.js" type="text/javascript"></script>
</head>
<body>
<!--[if lte IE 9]>
    <p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，AnBlog不在支持。 请<a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
<![endif]-->

<?php
    //加载头部
    require_once './include/admin_header.php';
    echo "<div class='am-cf admin-main'>";
    //加载侧边栏
    require_once './include/admin_sidebar.php';
?>
    <!-- content start -->
    <div class="admin-content">
        <div class="am-cf am-padding">
            <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">添加文章</strong> / <small>add blog</small></div>
        </div>
        <div class="am-tabs am-margin" data-am-tabs>
            <div class="am-tabs-bd" style="border:0px;">
                <div class="am-tab-panel am-fade" id="tab2">
                    <form class="am-form" action="blog_doaction.php?act=addblog&pid=<?php echo $pid; ?>" method="post">
                        <div class="am-g am-margin-top">
                            <div class="am-u-sm-4 am-u-md-2 am-text-right">
                                文章标题
                            </div>
                            <div class="am-u-sm-8 am-u-md-4">
                                <input type="text" class="am-input-sm" name="title" value="<?php echo $rows['title']; ?>">
                            </div>
                            <div class="am-hide-sm-only am-u-md-6">*必填，不可重复</div>
                        </div>
                        <div class="am-g am-margin-top">
                            <div class="am-u-sm-4 am-u-md-2 am-text-right">
                                文章作者
                            </div>
                            <div class="am-u-sm-8 am-u-md-4 am-u-end col-end">
                                <input type="text" class="am-input-sm" name="uid" value="<?php echo $rows['uid']; ?>" disabled>
                            </div>
                        </div>
                        <div class="am-g am-margin-top">
                            <div class="am-u-sm-4 am-u-md-2 am-text-right">
                                文章标签
                            </div>
                            <div class="am-u-sm-8 am-u-md-4">
                                <input type="text" class="am-input-sm" name="tag" value="<?php echo $rows['tag']; ?>">
                            </div>
                            <div class="am-hide-sm-only am-u-md-6"></div>
                        </div>
                        <div class="am-g am-margin-top">
                            <div class="am-u-sm-4 am-u-md-2 am-text-right">
                                内容摘要
                            </div>
                            <div class="am-u-sm-8 am-u-md-4">
                                <input type="text" class="am-input-sm" name="about" value="<?php echo $rows['about']; ?>">
                            </div>
                            <div class="am-u-sm-12 am-u-md-6"></div>
                        </div>
                        <div class="am-g am-margin-top-sm">
                            <div class="am-u-sm-12 am-u-md-2 am-text-right admin-form-text">
                                文章正文
                            </div>
                            <div class="am-u-sm-12 am-u-md-10">
                                <textarea id="post_body" name="content" class="textarea" placeholder="这里请输入文章正文" value="<?php echo $rows['content']; ?>"></textarea>
                            </div>
                        </div>
                        <div class="am-g am-margin-top">
                            <div class="am-u-sm-4 am-u-md-2 am-text-right">
                                保存类型
                            </div>
                            <div class="am-u-sm-8 am-u-md-4">
                              <label>
                                <input type="radio" name="isdraft" value="1" <?php if ($rows['isdraft'] == 1) echo "checked"; ?>>
                                草稿
                              </label>
                            </div>

                            <div class="am-radio-inline">
                              <label>
                                <input type="radio" name="isdraft" value="0"  <?php if ($rows['isdraft'] == 0) echo "checked"; ?>>
                                正文
                              </label>
                            </div>
                        </div>
                        <div class="am-margin">
                            <button type="submit" class="am-btn am-btn-primary am-btn-xs">提交</button>
                            <button type="reset" class="am-btn am-btn-primary am-btn-xs">重置</button>
                        </div>
                    </form>
                    <script type="text/javascript">
                        $("#post_body").qeditor({});
                        
                        // Custom a toolbar icon
                        var toolbar = $("#post_body").parent().find(".qeditor_toolbar");
                        var link = $("<a href='#'><span class='icon-smile' title='smile'></span></a>");
                        link.click(function(){
                          alert("Put you custom toolbar event in here.");
                          return false;
                        });
                        toolbar.append(link);
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!-- content end -->
<?php require_once './admin_frame_end.php'; ?>