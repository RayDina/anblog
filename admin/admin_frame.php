<?php
/**
 * Created by PhpStorm.
 * User: wangbo
 * Date: 2015/6/9
 * Time: 9:51
 */
//权限判断
require_once 'session.inc.php';
require_once '../config/config.php';
require_once '../include/db.class.php';


 ?>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AnBlog Manager System</title>
  <meta name="description" content="AnBlog Manager System">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="../assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="../assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="../assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="../assets/css/admin.css">
  <link rel="stylesheet" href="../assets/css/app.css">
</head>
<body>
<!--[if lte IE 9]>
    <p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，AnBlog不在支持。 请<a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
<![endif]-->

<?php
    //加载头部
    require_once './include/admin_header.php';
    echo "<div class='am-cf admin-main'>";
    //加载侧边栏
    require_once './include/admin_sidebar.php';
?>