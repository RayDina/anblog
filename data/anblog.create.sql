SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+08:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `anblog` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `anblog`;

CREATE TABLE `an_group` (
  `gid` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `webmg` tinyint(1) NOT NULL DEFAULT '0',
  `linkmg` tinyint(1) NOT NULL DEFAULT '0',
  `blogmg` tinyint(1) NOT NULL DEFAULT '0',
  `usermg` tinyint(1) NOT NULL DEFAULT '0',
  `groupmg` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户组权限表';


CREATE TABLE `an_links` (
  `lid` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `url` varchar(200) NOT NULL,
  `other` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='友情链接';


CREATE TABLE `an_posts` (
  `pid` int(8) NOT NULL AUTO_INCREMENT,
  `uid` int(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `about` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `isdraft` tinyint(1) NOT NULL DEFAULT '0' COMMENT '草稿？',
  `date` date NOT NULL DEFAULT '2015-01-01',
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='博文信息表';


CREATE TABLE `an_user` (
  `uid` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `mail` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `about` varchar(250) DEFAULT NULL,
  `gid` int(4) NOT NULL DEFAULT '9',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `mail` (`mail`),
  KEY `user_group` (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COMMENT='用户数据';
