-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 06 月 19 日 10:38
-- 服务器版本: 5.5.40
-- PHP 版本: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `anblog`
--

-- --------------------------------------------------------

--
-- 表的结构 `an_comment`
--

CREATE TABLE IF NOT EXISTS `an_comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `time` datetime NOT NULL COMMENT '评论时间',
  `name` varchar(30) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `text` varchar(200) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='评论表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `an_comment`
--

INSERT INTO `an_comment` (`cid`, `pid`, `time`, `name`, `mail`, `text`) VALUES
(1, 1, '2015-06-02 03:12:07', 'idina', 'wangbo@xiyoulinux.org', '不错不错，赞一个~~~'),
(2, 2, '2015-06-19 06:05:31', '校长', '869780307@qq.com', 'Http定义了与服务器交互的不同方法，最基本的方法有4种，分别是GET，POST，PUT，DELETE。URL全称是资源描述符，我们可以这样认为：一个URL地址，它用于描述一个网络上的资源，而HTTP中的GET，POST，PUT，DELETE就对应着对这个资源的查，改，增，删4个操作。到这里，大家应该有个大概的了解了，GET一般用于获取/查询资源信息，而POST一般用于更新资源信息。'),
(3, 1, '2015-06-19 10:15:16', '霄霄', '869780307@qq.com', '好腻害~~~~~~~~~~~~~~~');

-- --------------------------------------------------------

--
-- 表的结构 `an_links`
--

CREATE TABLE IF NOT EXISTS `an_links` (
  `lid` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `url` varchar(200) NOT NULL,
  `other` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='友情链接' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `an_links`
--

INSERT INTO `an_links` (`lid`, `title`, `url`, `other`) VALUES
(1, 'idina', 'http://www.idina.cn', '');

-- --------------------------------------------------------

--
-- 表的结构 `an_posts`
--

CREATE TABLE IF NOT EXISTS `an_posts` (
  `pid` int(8) NOT NULL AUTO_INCREMENT,
  `uid` int(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `about` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `isdraft` tinyint(1) NOT NULL DEFAULT '0' COMMENT '草稿？',
  `date` date NOT NULL DEFAULT '2015-01-01',
  `tag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='博文信息表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `an_posts`
--

INSERT INTO `an_posts` (`pid`, `uid`, `title`, `about`, `content`, `isdraft`, `date`, `tag`) VALUES
(1, 1000, '博客文章样例', '<p>Blog就是以网络作为载体，简易迅速便捷地发布自己的心得，及时有效轻松地与他人进行交流，再集丰富多彩的个性化展示于一体的综合性平台。</p>            <p>在网络上发表Blog的构想始于1998年，但到了2000年才开始真正流行。</p>            <p>2000年博客开始进入中国，并迅速发展，但都业绩平平。</p>', '<p>Blog就是以网络作为载体，简易迅速便捷地发布自己的心得，及时有效轻松地与他人进行交流，再集丰富多彩的个性化展示于一体的综合性平台。</p>\r\n\r\n          <p>在网络上发表Blog的构想始于1998年，但到了2000年才开始真正流行。</p>\r\n\r\n          <p>2000年博客开始进入中国，并迅速发展，但都业绩平平。</p><p>Blog就是以网络作为载体，简易迅速便捷地发布自己的心得，及时有效轻松地与他人进行交流，再集丰富多彩的个性化展示于一体的综合性平台。</p>\r\n\r\n          <p>在网络上发表Blog的构想始于1998年，但到了2000年才开始真正流行。</p>\r\n\r\n          <p>2000年博客开始进入中国，并迅速发展，但都业绩平平。</p><p>Blog就是以网络作为载体，简易迅速便捷地发布自己的心得，及时有效轻松地与他人进行交流，再集丰富多彩的个性化展示于一体的综合性平台。</p>\r\n\r\n          <p>在网络上发表Blog的构想始于1998年，但到了2000年才开始真正流行。</p>\r\n\r\n          <p>2000年博客开始进入中国，并迅速发展，但都业绩平平。</p>', 0, '2015-06-18', '博客'),
(2, 1000, '博客文章样例2', '博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一', '博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一些博客专注在艺术、摄影、视频、音乐、播客等各种主题。博客是社会媒体网络的一部分。比较著名的有新浪、网易等博客。博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一些博客专注在艺术、摄影、视频、音乐、播客等各种主题。博客是社会媒体网络的一部分。比较著名的有新浪、网易等博客。博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一些博客专注在艺术、摄影、视频、音乐、播客等各种主题。博客是社会媒体网络的一部分。比较著名的有新浪、网易等博客。\r\n博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一些博客专注在艺术、摄影、视频、音乐、播客等各种主题。博客是社会媒体网络的一部分。比较著名的有新浪、网易等博客。博客，仅音译，它的正式名称为网络日志；又音译为部落格或部落阁等，是一种通常由个人管理、不定期张贴新的文章的网站。博客上的文章通常根据张贴时间，以倒序方式由新到旧排列。许多博客专注在特定的课题上提供评论或新闻，其他则被作为比较个人的日记。一个典型的博客结合了文字、图像、其他博客或网站的链接及其它与主题相关的媒体，能够让读者以互动的方式留下意见，是许多博客的重要要素。大部分的博客内容以文字为主，仍有一些博客专注在艺术、摄影、视频、音乐、播客等各种主题。博客是社会媒体网络的一部分。比较著名的有新浪、网易等博客。', 0, '2015-01-23', '博客,blog,'),
(3, 1000, 'mysqli 教程', 'mysqli提供了面向对象和面向过程两种方式来与数据库交互，分别看一下这两种方式。', '<div>mysqli提供了面向对象和面向过程两种方式来与数据库交互，分别看一下这两种方式。</div><div><br></div><div>1．面向对象</div><div>在面向对象的方式中，mysqli被封装成一个类，它的构造方法如下：</div><div>__construct ([ string $host [, string $username [, string $passwd [, string $dbname</div><div>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>[, int $port [, string $socket ]]]]]] )&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr></div><div>在上述语法中涉及到的参数说明如下。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;host：连接的服务器地址。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;username：连接数据库的用户名，默认值是服务器进程所有者的用户名。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;passwd：连接数据库的密码，默认值为空。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;dbname：连接的数据库名称。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;port：TCP端口号。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;socket：UNIX域socket。</div><div><br></div><div><div>2．面向过程</div><div>&nbsp;<wbr></div><div>在面向过程的方式中，mysqli扩展提供了函数mysqli_connect()与MySQL建立连接，该函数的语法格式如下：</div><div>mysqli mysqli_connect ([ string $host [, string $username [, string $passwd</div><div>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;&nbsp;<wbr>&nbsp;<wbr>&nbsp;[, string $dbname [, int $port [, string $socket ]]]]]] )</div><div>mysqli_connect()函数的用法与mysql扩展中的mysql_connect()函数用法十分相似。</div></div><div><br></div><div>3 &nbsp;<wbr>使用mysqli存取数据<br></div><div><div><br></div><div>使用mysqli存取数据也包括面向对象和面向过程两种方式，在本节我们只讨论如何使用面向对象的方式来与MySQL交互，关于mysqli扩展中使用面向过程方式这里就不再详细介绍了，有兴趣的读者可参考官方文档来获取相关的资料。</div><div>在mysqli中，执行查询使用query()方法，该方法的语法格式如下：</div><div>mixed query ( string $query [, int $resultmode ] )</div><div>在上述语法中涉及到的参数说明如下。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;query：向服务器发送的SQL语句。</div><div>l&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;<wbr>&nbsp;resultmode：该参数接受两个值，一个是MYSQLI_STORE_RESULT，表示结果作为缓冲集合返回；另一个是MYSQLI_USE_RESULT，表示结果作为非缓冲集合返回。</div></div>', 1, '2015-06-17', 'mysqli'),
(4, 1000, 'CSS 盒模型', 'CSS 盒模型  曾几何时，IE 6 及更低版本的非标准盒模型被喷得体无完肤。IE 原来的盒模型真的不好么？最终，时间给了另外一个答案。 W3C 终认识到所谓标准盒模型在实际使用中的复杂性，于是在 CSS3 中增加 box-sizing 这一属性，允许用户自定义盒模型。', '<h2>CSS 盒模型<a href="http://localhost/docs-gh-pages/2.x/css/base/#css-%E7%9B%92%E6%A8%A1%E5%9E%8B" title="Heading anchor"></a></h2><p>曾几何时，IE 6 及更低版本的<em>非标准</em>盒模型被喷得体无完肤。IE 原来的盒模型真的不好么？最终，时间给了另外一个答案。 W3C 终认识到所谓标准盒模型在实际使用中的复杂性，于是在 CSS3 中增加&nbsp;box-sizing&nbsp;这一属性，允许用户自定义盒模型。</p><blockquote><p>You tell me I''m wrong, Then you better prove you''re right.</p><p>King of Pop – Scream</p></blockquote><p>这就是 W3C 的证明。</p><p>扯远了，Amaze UI 将所有元素的盒模型设置为&nbsp;border-box。这下好了，妈妈再也不用担心你没计算好padding、border&nbsp;而使布局破相了。咱来写样式，不是来学算术。</p><div><div>&nbsp;Copy</div></div><div> *,\r\n *:before,\r\n *:after {\r\n   -moz-box-sizing: border-box;\r\n   -webkit-box-sizing: border-box;\r\n   box-sizing: border-box;\r\n }</div>', 0, '2015-06-19', 'CSS,盒模型'),
(5, 1000, '字号及单位', 'Amaze UI 将浏览器的基准字号设置为 62.5%，也就是 10px，现在 1rem = 10px —— 为了计算方便。然后在body 上应用了 font-size: 1.6rem;，将页面字号设置为 16px。  html {   font-size: 62.5%; }  body {   font-size: 1.6rem; /* =16px */ } 与 em 根据上下文变化不同，rem', '<h3>Amaze UI 将浏览器的基准字号设置为&nbsp;62.5%，也就是&nbsp;10px，现在&nbsp;1rem = 10px&nbsp;—— 为了计算方便。然后在body&nbsp;上应用了&nbsp;font-size: 1.6rem;，将页面字号设置为&nbsp;16px。</h3><pre>html {\r\n  font-size: 62.5%;\r\n}\r\n\r\nbody {\r\n  font-size: 1.6rem; /* =16px */\r\n}</pre><p>与&nbsp;em&nbsp;根据上下文变化不同，rem&nbsp;只跟基准设置关联，只要修改基准字号，所有使用&nbsp;rem&nbsp;作为单位的设置都会相应改变。</p><p>当然，并非所有所有浏览器的默认字号都是&nbsp;16px，所以在不同的浏览器上会有细微差异。</p><p>另外，一些需要根据字号做相应变化的场景也使用了&nbsp;em，需要像素级别精确的场景也使用了&nbsp;px.</p>', 0, '2015-06-19', 'css');

-- --------------------------------------------------------

--
-- 表的结构 `an_user`
--

CREATE TABLE IF NOT EXISTS `an_user` (
  `uid` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `mail` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `about` varchar(250) DEFAULT NULL,
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `mail` (`mail`),
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户数据' AUTO_INCREMENT=1019 ;

--
-- 转存表中的数据 `an_user`
--

INSERT INTO `an_user` (`uid`, `name`, `sex`, `mail`, `password`, `about`, `isadmin`) VALUES
(1000, 'root', 0, 'wangbo@xiyoulinux.org', '670b14728ad9902aecba32e22fa4f6bd', '', 1),
(1014, 'sdsdsds', 1, 'wangbsdsdo@xiyoulinux.org', 'e004202ff1930400fd8302cdc49523e3', 'dsds', 0),
(1015, 'nnnnnssssss', 1, 'wangbo@xiyoulinux.orgnnn', 'fad9527a3f157b64e306282ba816df36', 'nnnssss', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
