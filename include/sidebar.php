
  <div class="am-u-md-4 blog-sidebar">
    <div class="am-panel-group">
      <section class="am-panel am-panel-default">
        <div class="am-panel-hd">Anblog公告</div>
        <div class="am-panel-bd">
          <p>Anblog开源博客系统开放测试啦~~</p>
          <ul>
              <li><i class="am-icon-qq am-icon-fw"></i> QQ</li>
              <li><i class="am-icon-skype am-icon-fw"></i> Skype</li>
              <li><i class="am-icon-github am-icon-fw"></i> GitHub</li>
          </ul>
          <a class="am-btn am-btn-success am-btn-sm" href="#">查看更多 →</a>
        </div>
      </section>
      <section class="am-panel am-panel-default">
          
        <div class="am-panel-hd"><i class="am-icon-book am-icon-fw"></i> 文章目录</div>
        <ul class="am-list blog-list">
        <?php
          require_once "db.class.php";
          require_once "./config/config.php";
          if(!isset($db)){
            $db = new an_db();
          }
          $result = $db->query("SELECT pid,title from an_posts where isdraft=0");
          if($result && $result->num_rows>0) {
            while ($rows=$result->fetch_assoc()) {
                echo "<li><a href='info.php?pid={$rows['pid']}'>{$rows['title']}</a></li>";
            }
          }else{
            echo "博客还没有添加文章哦~~";
          }
        ?>
        </ul>
      </section>

      <section class="am-panel am-panel-default">
        <div class="am-panel-hd">友情链接</div>
        <div class="am-panel-bd">
          <ul class="am-avg-sm-4 blog-team">
            <?php
              $result = $db->query("SELECT * from an_links");
              if($result && $result->num_rows>0) {
                while ($rows=$result->fetch_assoc()) {
                  echo "<li><a href='{$rows['url']}' target='_black'>{$rows['title']}</a></li>";
                }
              }else{
                echo "博客还没有添加友情链接哦~~";
              }
            ?>
          </ul>
        </div>
      </section>
    </div>
  </div>

</div>
