<?php
/**
 * Created by PhpStorm.
 * User: wangbo
 * Date: 2015/6/10
 * Time: 23:02
 */
//数据库类

class an_db {
    //数据库连接信息
    private $db;
    private $res;

    function __construct()
    {
        $this->db = new mysqli(AN_DB_HOST, AN_DB_USER, AN_DB_PASSWORD, AN_DB_NAME, AN_DB_PORT);
        if (mysqli_connect_errno())
        {
            echo "数据库连接失败".mysqli_connect_errno().mysqli_connect_error();
            exit();
        }
        $this->db->set_charset('utf8');
        return $this->db;
    }

    public function query($sql)
    {
        $this->res = $this->db->query($sql);
        return $this->res;
        //fetch_assoc
    }

    //字符过滤
    public function escape_string($buf)
    {
        return $this->db->escape_string($buf);
    }

    
    function __destruct()
    {
        if( is_object($this->res) )
            $this->res->free();
        $this->db->close();
    }
}
?>
