<header class="am-topbar am-topbar-fixed-top">
  <h1 class="am-topbar-brand">
    <a href="#">
      <?php 
          if(isset($SiteName))
            echo $SiteName;
          else{
             echo "Anblog";
           } 
      ?>
    </a>
  </h1>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only"
          data-am-collapse="{target: '#doc-topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span
      class="am-icon-bars"></span></button>

  <div class="am-collapse am-topbar-collapse" id="doc-topbar-collapse">
    <ul class="am-nav am-nav-pills am-topbar-nav">
      <li class="am-active"><a href="index.php">主页</a></li>
            <li class="am-dropdown" data-am-dropdown>
        <a class="am-dropdown-toggle" data-am-dropdown-toggle href="javascript:;">
          文章 <span class="am-icon-caret-down"></span>
        </a>
        <ul class="am-dropdown-content">
          <li class="am-dropdown-header">IT</li>
          <li><a href="#">Win编程</a></li>
          <li><a href="#">Linux开发</a></li>
          <li><a href="#">算法分析</a></li>
        </ul>
      </li>
      <li><a href="#">关于我们</a></li>
      <li><a href="./login.php">登陆</a></li>
    </ul>

    <form class="am-topbar-form am-topbar-left am-form-inline am-topbar-right" role="search">
      <div class="am-form-group">
        <input type="text" class="am-form-field am-input-sm" placeholder="搜索文章">
      </div>
      <button type="submit" class="am-btn am-btn-default am-btn-sm">搜索</button>
    </form>

  </div>
</header>