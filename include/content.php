<body>
    <div class="am-g am-g-fixed blog-g-fixed">
        <div class="am-u-md-8">
            <?php
            //content.php
            //加载文章主要信息，分页处理等

            //页码判断
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }else{
                $page = 0;
            }

            $db = new an_db();
            $sql = "select an_posts.pid,an_posts.title,an_user.name,an_user.uid,an_posts.about,an_posts.date from an_user inner join an_posts on an_posts.uid = an_user.uid and an_posts.isdraft=0 LIMIT ".($page*10).", 10; ";
            $result = $db->query($sql);
            if($result){
                //判断结果集中数目是不是大于0
                if ($result && $result->num_rows > 0){
                  //fetch_assoc
                    while ($rows = $result->fetch_assoc()) {
            ?>
                <article class="blog-main">
                    <h3 class="am-article-title blog-title">
                        <a href="info.php?pid=<?php echo $rows['pid']; ?>"><?php echo $rows['title']; ?></a>
                    </h3>
                    <h4 class="am-article-meta blog-meta">作者： <a href="info.php?uid=<?php echo $rows['uid']; ?>"><?php echo $rows['name']; ?></a> 时间：<?php echo $rows['date']; ?> </h4>
                    <div class="am-g blog-content">
                        <div class="am-u-lg-12">
                          <?php echo $rows['about']; ?>
                        </div>
                    </div>
                </article>
            </div>
            <hr class="am-article-divider blog-hr"/>
            <?php
                    }
                }else{
                    echo "数据库暂时没有博客信息~~~";
                }
            }
            if ($result->num_rows > 10 ) { 
            ?>
                <ul class="am-pagination blog-pagination">
                    <li class="am-pagination-prev <?php if ($page == 0) echo "am-disabled"; ?>"><a href="?page=<?php echo $page-1; ?>">&laquo; 上一页</a></li>
                    <li class="am-pagination-next"><a href="?page=<?php echo $page+1; ?>">下一页 &raquo;</a></li>
                </ul>
            <?php } ?>
    </div>
