<?php

header("content-type:text/html;charset=utf-8");

date_default_timezone_set("PRC");

if(!isset($_SESSION)){
	session_start();
}

define("ROOT",dirname(__FILE__));

set_include_path(".".PATH_SEPARATOR.ROOT."/include".PATH_SEPARATOR.ROOT."/admin/include".PATH_SEPARATOR.ROOT."/config".PATH_SEPARATOR.get_include_path());

// 载入配置
if (!@include_once './config/config.php') {
    file_exists('./install.php') ? header('Location: install.php') : print('Missing Config File');
    exit;
}


require_once "./config/config.php";
require_once 'db.class.php';
require_once 'string.fun.php';
/*require_once "configs.php";
*/

